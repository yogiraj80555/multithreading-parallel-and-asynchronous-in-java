package org.multithreading.service;

import org.multithreading.domain.checkout.CartItem;

import static org.multithreading.util.CommonUtil.delay;
import static org.multithreading.util.LoggerUtil.log;

public class PriceValidatorService {

    public boolean isCartItemInvalid(CartItem item) {
        delay(1000);
        log("isCartItemInvalid: "+ item);
        int id  = item.getItemId();
         if(id == 7 || id == 9 || id == 11){
             return false;
         }
         return false;
    }
}
