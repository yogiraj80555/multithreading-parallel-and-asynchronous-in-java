package org.multithreading.service;

import org.multithreading.domain.Product;
import org.multithreading.domain.ProductInfo;
import org.multithreading.domain.Review;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

import static org.multithreading.util.CommonUtil.stopWatch;
import static org.multithreading.util.LoggerUtil.log;

public class ProductService {
    private ProductInfoService productInfoService;
    private ReviewService reviewService;

    public ProductService(ProductInfoService productInfoService, ReviewService reviewService) {
        this.productInfoService = productInfoService;
        this.reviewService = reviewService;
    }

    public Product retriveProductDetails(String productId) {

        stopWatch.start();

        //pros: this kind of code is very initiative and synchronous, Execuation of code goes step by step form top to bottom
        //cons: this kind of code is blocking nature mean that review service can only call after finishing product info call even though they both dependent on each other.
        //here we can improve the performance.
        ProductInfo productInfo = productInfoService.retrieveProductInfo(productId); //blocking call
        Review review = reviewService.retrieveReviews(productId); //blocking call


        stopWatch.stop();
        log("Total Time Taken : "+ stopWatch.getTime());
        Product product = new Product(productId, productInfo, review);
        return product;

    }

    public static void main(String args []) {
        ProductInfoService infoService = new ProductInfoService();
        ReviewService reviewService = new ReviewService();
        ProductService productService = new ProductService(infoService,reviewService);
        String productId = "ABCD123";
        Product product = productService.retriveProductDetails(productId);
        log("Product is: "+ product);
    }

}
