package org.multithreading.service;

import org.multithreading.domain.checkout.Cart;
import org.multithreading.domain.checkout.CartItem;
import org.multithreading.domain.checkout.CheckoutResponse;
import org.multithreading.domain.checkout.CheckoutStatus;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.summingDouble;
import static org.multithreading.util.CommonUtil.startTimer;
import static org.multithreading.util.CommonUtil.timeTaken;
import static org.multithreading.util.LoggerUtil.log;

public class CheckoutService {

    private PriceValidatorService priceValidatorService;

    public CheckoutService(PriceValidatorService priceValidatorService) {
        this.priceValidatorService = priceValidatorService;
    }

    public CheckoutResponse checkout(Cart cart) {
        startTimer();
        List<CartItem> priceValidatorList = cart.getCartItemList()
                        .parallelStream()
                                .map((cartItem -> {
                                    boolean priceValidation = priceValidatorService.isCartItemInvalid(cartItem);
                                    cartItem.setExpired(priceValidation);
                                    return cartItem;
                                }))
                .filter(CartItem::isExpired)
                .collect(Collectors.toList());

        timeTaken();
        if(priceValidatorList.size() > 0){
            return new CheckoutResponse(CheckoutStatus.FAILURE, priceValidatorList);
        }

        double calculatedPrice = finalPrice(cart);
        double calculatedPrice1 = finalPriceUsingReduce(cart);
        log("Reduce price is: "+calculatedPrice1);

        return new CheckoutResponse(CheckoutStatus.SUCCESS, calculatedPrice);
    }


    private double finalPrice(Cart cart) {
        return cart.getCartItemList()
                .parallelStream()
                .map(cartItem -> cartItem.getQuantity() * cartItem.getRate())
                .collect(summingDouble(Double::doubleValue));
    }

    private double finalPriceUsingReduce(Cart cart) {
        return cart.getCartItemList()
                .parallelStream()
                .map(cartItem -> cartItem.getQuantity() * cartItem.getRate())
                .reduce(0.0, (x,y)->x+y);  //here reduce(0.0, Double::sum);
    }
}
