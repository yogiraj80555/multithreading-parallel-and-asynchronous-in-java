package org.multithreading.service;

import org.multithreading.domain.Product;
import org.multithreading.domain.ProductInfo;
import org.multithreading.domain.Review;

import static org.multithreading.util.CommonUtil.stopWatch;
import static org.multithreading.util.LoggerUtil.log;

public class ProductServiceUsingThread {
    private ProductInfoService productInfoService;
    private ReviewService reviewService;

    public ProductServiceUsingThread(ProductInfoService productInfoService, ReviewService reviewService) {
        this.productInfoService = productInfoService;
        this.reviewService = reviewService;
    }

    public Product retriveProductDetails(String productId) {

        stopWatch.start();

        // below code gives you more performance but this is to lenghty code to write also runnable not suitable to get back return value.
        ProductInfoRunnable productInfoRunnable = new ProductInfoRunnable(productId);
        Thread productInfoThread = new Thread(productInfoRunnable);

        ReviewRunnable reviewRunnable = new ReviewRunnable(productId);
        Thread reviewThread = new Thread(reviewRunnable);

        productInfoThread.start();
        reviewThread.start();

        try {
            productInfoThread.join(); //join stop this current(main) thread till productInfoThread will not get completed.
            reviewThread.join(); //join stop this current(main) thread till reviewThread will not get completed.
        } catch (InterruptedException e) {
            log("Exception during thread join : "+ e.getMessage());
        }
        ProductInfo productInfo = productInfoRunnable.getProductInfo();
        Review review = reviewRunnable.getReview();
        stopWatch.stop();
        log("Total Time Taken : "+ stopWatch.getTime());
        Product product = new Product(productId, productInfo, review);
        return product;

    }

    public static void main(String args []) {
        ProductInfoService infoService = new ProductInfoService();
        ReviewService reviewService = new ReviewService();
        ProductServiceUsingThread productService = new ProductServiceUsingThread(infoService,reviewService);
        String productId = "ABCD123";
        Product product = productService.retriveProductDetails(productId);
        log("Product is: "+ product);
    }

    private class ProductInfoRunnable implements Runnable {
        //Note: Runnable is that he doesn't take any input and it doesn't return any output. So the only way to parse the input and only way to get the input is
        // by introducing additional properties
        private ProductInfo productInfo;
        private String productId;
        public ProductInfoRunnable(String productId) {
            this.productId = productId;
        }

        public ProductInfo getProductInfo() {
            return productInfo;
        }

        @Override
        public void run() {
            productInfo = productInfoService.retrieveProductInfo(productId);
        }
    }

    private class ReviewRunnable implements Runnable {
        String productId;
        Review review;
        public ReviewRunnable(String productId) {
            this.productId = productId;
        }

        public Review getReview() {
            return review;
        }

        @Override
        public void run() {
            review = reviewService.retrieveReviews(productId);
        }
    }
}
