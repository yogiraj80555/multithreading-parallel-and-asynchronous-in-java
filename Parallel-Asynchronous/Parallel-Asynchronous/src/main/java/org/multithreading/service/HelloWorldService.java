package org.multithreading.service;

import org.multithreading.completablefuture.CompletableFeatureHelloWorld;

import java.util.concurrent.CompletableFuture;

import static org.multithreading.util.CommonUtil.delay;
import static org.multithreading.util.LoggerUtil.log;

public class HelloWorldService {

    public String helloWorld() {
        delay(1000);
        log("Inside Hello World");
        return "Hello World!";
    }

    public String hello() {
        delay(1000);
        log("Inside Hello");
        return "hello ";
    }

    public String world() {
        delay(1000);
        log("inside world");
        return "World!";
    }


    public CompletableFuture<String> worldFuture(String input){
        return CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return input + "World!";
        });
    }

}
