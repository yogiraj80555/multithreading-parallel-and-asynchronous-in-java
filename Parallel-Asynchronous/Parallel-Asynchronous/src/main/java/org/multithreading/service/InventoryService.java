package org.multithreading.service;

import org.multithreading.domain.Inventory;
import org.multithreading.domain.ProductOption;

import static org.multithreading.util.CommonUtil.delay;

public class InventoryService {
    public Inventory retriveInventory(ProductOption option) {
        delay(1000);
        return Inventory.builder().count(2).build();
    }
}
