package org.multithreading.service;

import org.multithreading.domain.Review;
import org.multithreading.util.LoggerUtil;

import static org.multithreading.util.CommonUtil.delay;

public class ReviewService {
    public Review retrieveReviews(String productId) {
        delay(1000);
        LoggerUtil.log("RetrieveReviews after Delay");
        return new Review(250,4.8);
    }
}
