package org.multithreading.parallelStreams;

import org.multithreading.util.Dataset;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.multithreading.util.CommonUtil.stopWatch;
import static org.multithreading.util.CommonUtil.timeTaken;
import static org.multithreading.util.CommonUtil.delay;
import static org.multithreading.util.LoggerUtil.log;

public class ParallelStreamsExample {
    public static void main(String[] args) {

        List<String> names = Dataset.namedList();
        ParallelStreamsExample parallelStreamsExample = new ParallelStreamsExample();
        stopWatch.start();
        List<String> nameTransforms = parallelStreamsExample.transformUsingSequentialStream(names);
        timeTaken();
        log("Sequential Stream Result List: " + nameTransforms +"\n\nResetting Stop Watch......");
        stopWatch.reset();
        stopWatch.start();
        nameTransforms = parallelStreamsExample.transformUsingParallelStream(names);
        timeTaken();
        log("Parallel Stream Result List: " + nameTransforms);


    }

    public List<String> transformUsingSequentialStream(List<String> names){
        return names
                .stream()
                .map(this::nameLenghtTransform) //In Streams, API, any kind of transformation that we want to play on, each and every element, you would use a map function.
                .parallel()                     //here last function call select overall all pipeline behaviour
                .collect(Collectors.toList());
    }

    public List<String> transformUsingParallelStream(List<String> names){
        return names
                .parallelStream()
                .map(this::nameLenghtTransform) //In Streams, API, any kind of transformation that we want to play on, each and every element, you would use a map function.
                .sequential()                   //here last function call select overall all pipeline behaviour
                .collect(Collectors.toList());
    }

    //Dynamic sequential or parallel
    public List<String> transformDynamic_Sequential_Parallel(List<String> names, boolean isParallel){
        Stream<String> namesStream = isParallel ? names.stream():names.parallelStream();


        return namesStream
                .map(this::nameLenghtTransform)
                .collect(Collectors.toList());
    }

    private String nameLenghtTransform(String name){
        delay(1000);
        return name + " --- "+name.length();
    }
}
