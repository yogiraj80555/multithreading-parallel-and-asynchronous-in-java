package org.multithreading.parallelStreams;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.multithreading.util.LoggerUtil.log;

public class ParallelStreamResultOrder {


    public static List<Integer> orderResult(List<Integer> numbers) {
        return numbers.parallelStream().map((num) -> num * 2).collect(Collectors.toList());
    }

    public static Set<Integer> unorderResult(Set<Integer> numbers) {
        return numbers.parallelStream().map((num) -> num * 2).collect(Collectors.toSet());
    }

    public static void main(String[] args) {
        List<Integer> numbers =  List.of(1,2,3,4,5,6,7,8,9);
        log("Input: "+numbers);
        List<Integer> orderList = orderResult(numbers);
        log("Ordered result:"+orderList);

        Set<Integer> inputSet = Set.of(1,2,3,4,5,6,7,8,9);
        log("Input: "+inputSet);
        Set<Integer> unorderSet = unorderResult(inputSet);
        log("Unordered result: "+unorderSet);

    }
}
