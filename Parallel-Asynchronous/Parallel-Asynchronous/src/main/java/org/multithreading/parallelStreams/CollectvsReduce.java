package org.multithreading.parallelStreams;

import org.multithreading.util.Dataset;

import java.util.List;
import java.util.stream.Collectors;

import static org.multithreading.util.CommonUtil.*;
import static org.multithreading.util.LoggerUtil.log;

public class CollectvsReduce {

    public static String collect() {
        List<String> list = Dataset.namedList();
        return list
                .parallelStream()
                .collect(Collectors.joining());   //here collect save the memory
    }

    public static String reduce() {
        List<String> list = Dataset.namedList();
        return list
                .parallelStream()
                .reduce("",(s1,s2) -> s1+s2); //here we are creating bunch of string to perform string concatenation.
    }

    public static void main(String[] args) {
        startTimer();
        log("Using Collect: "+ collect());
        timeTaken();
        stopWatchReset();

        startTimer();
        log("Using reduce: "+ reduce());
        timeTaken();
    }
}
