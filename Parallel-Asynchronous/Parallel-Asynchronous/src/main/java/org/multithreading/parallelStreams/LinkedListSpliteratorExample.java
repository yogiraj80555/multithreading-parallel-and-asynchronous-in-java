package org.multithreading.parallelStreams;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.multithreading.util.CommonUtil.startTimer;
import static org.multithreading.util.CommonUtil.timeTaken;

public class LinkedListSpliteratorExample {

    public List<Integer> multiplayEachValue(LinkedList<Integer> numbres, int multiplayValue, boolean isParallel){

        Stream<Integer> integerStream = numbres.stream();

        if (isParallel){
            integerStream = integerStream.parallel();
        }
        startTimer();
        List<Integer> multiplyList =  integerStream
                .map((number) -> number * multiplayValue)
                .collect(Collectors.toList());
        timeTaken();

        return multiplyList;

    }
}
