package org.multithreading.apiclient;


import org.multithreading.domain.movie.Movie;
import org.multithreading.domain.movie.MovieInfo;
import org.multithreading.domain.movie.Review;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class MoviesClient {

    private WebClient webClient;

    public MoviesClient(WebClient webClient) {
        this.webClient = webClient;
    }


    public Movie retriveMovie(Long movieId) {
        //Swagger url:
        //  http://127.0.0.1:8080/movies/webjars/swagger-ui/index.html?configUrl=/movies/v3/api-docs/swagger-config#/
        //  for this you need jar located in lib folder of current project
        //  command to run jar:  java -jar <jar-name>.jar

        //invoke MovieInfo
        var movieInfo = invokeMovieInfoService(movieId);
        //invoke reviewList
        var reviewList = invokeReviewService(movieId);
        return new Movie(movieInfo, reviewList);
    }


    public List<Movie> retrieveMovies(List<Long> movieIds) {
        return movieIds
                .stream()
                .map(this::retriveMovie)
                .collect(Collectors.toList());
    }


    public CompletableFuture<Movie> retriveMovie_with_CompletableFuture(Long movieId) {

        //invoke MovieInfo
        var movieInfo = CompletableFuture.supplyAsync(()-> invokeMovieInfoService(movieId));
        //invoke reviewList
        var reviewList = CompletableFuture.supplyAsync(()-> invokeReviewService(movieId));


        return movieInfo
                .thenCombine(reviewList, (prev,current)-> {
                    return new Movie(prev,current);
                });

        //    OR we can write as

//        return movieInfo
//                .thenCombine(reviewList, Movie::new);
    }


    public List<Movie> retriveMovie_with_CompletableFuture(List<Long> movieIds){
        var moviesfeatures = movieIds
                .stream()
                .map(this::retriveMovie_with_CompletableFuture)
                .collect(Collectors.toList());

        return moviesfeatures
                .stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public List<Movie> retriveMovie_with_CompletableFuture_allOf(List<Long> movieIds){
        var moviesfeatures = movieIds
                .stream()
                .map(this::retriveMovie_with_CompletableFuture)
                .collect(Collectors.toList());

        CompletableFuture feature = CompletableFuture.allOf(moviesfeatures.toArray(new CompletableFuture[moviesfeatures.size()]));


        return (List<Movie>) feature
                .thenApply(void_val -> moviesfeatures
                        .stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList()))

                        .join();
    }







    private List<Review> invokeReviewService(Long movieId){

        String reviewService = UriComponentsBuilder.fromUriString("/v1/reviews")
                .queryParam("movieInfoId", movieId)
                .buildAndExpand()
                .toString();

        return webClient
                .get() //get request method
                .uri(reviewService) //web url
                .retrieve() //actually retrieving data from network
                .bodyToFlux(Review.class) //converting retrieve data to multiple class objects, It's an only for list of Objects
                .collectList() //collecting all objects into List which retrieves from bodyToFlux.
                .block(); //blocking current thread for completing pipeline, thus it will return actual object
    }



    private MovieInfo invokeMovieInfoService(Long movieId){

        var movieInfoUrl = "/v1/movie_infos/{movieId}";

        return webClient
                .get() //get request method
                .uri(movieInfoUrl,movieId) //web url
                .retrieve() //actually retrieving data from network
                .bodyToMono(MovieInfo.class) //converting retrieve data to class , It's and only for single Object
                .block(); //blocking current thread for completing pipeline, thus it will return actual object
    }
}
