package org.multithreading.domain.checkout;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data   //creating all getters and setters
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cart {

    private Integer cardId;
    private List<CartItem> cartItemList;
}
