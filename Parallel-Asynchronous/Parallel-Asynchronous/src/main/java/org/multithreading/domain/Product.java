package org.multithreading.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@AllArgsConstructor
@Data
public class Product {
    @NonNull
    private String productId;

    @NonNull
    private ProductInfo productInfo;

    @NonNull
    private Review review;
}
