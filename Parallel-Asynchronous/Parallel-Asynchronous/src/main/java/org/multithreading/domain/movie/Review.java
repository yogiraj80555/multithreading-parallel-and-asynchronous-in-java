package org.multithreading.domain.movie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Review {
    String reviewId;
    String movieInfoId;
    String rating;
    String comment;
}
