package org.multithreading.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor //this creates all arguments constructor.
@Builder //use to help build the product info <- instead here we can use constructor injection also.
@NoArgsConstructor //it takes care generating a constructor with no arguments.
@Data  //it take care of generating getter, setters, and toString() for us.
public class ProductInfo {
    private String productId;
    private List<ProductOption> productOptions;
}
