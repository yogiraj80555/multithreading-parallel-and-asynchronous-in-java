package org.multithreading.domain.checkout;

public enum CheckoutStatus {
    SUCCESS,
    FAILURE
}
