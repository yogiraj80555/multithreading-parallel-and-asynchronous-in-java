package org.multithreading.domain.movie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieInfo {
    String movieInfoId;
    String name;
    String year;
    List<String> cast;
    String release_date;
}
