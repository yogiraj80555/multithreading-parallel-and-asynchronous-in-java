package org.multithreading.completablefuture;

import org.multithreading.service.HelloWorldService;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.multithreading.util.CommonUtil.*;
import static org.multithreading.util.LoggerUtil.log;

public class CompletableFeatureHelloWorld {
    public static void main(String[] args) {
        HelloWorldService helloWorldService = new HelloWorldService();

        //here our main program/thread get completed before completable future completed therefore "Result ...." won't get printed.
        CompletableFuture.supplyAsync(() -> helloWorldService.helloWorld())
                // thenAccept is going to give you the access to the value that's inside the completable future.
                .thenAccept((result) -> {
                    log("Result: " + result);
                })/*.join()    <- this use to see Async output, it will block main thread till future get executes*/;

        log("Done.");

        //here adding delay of 2 sec so completable future get time to execute itself and print the result.
        delay(2000);


        CompletableFuture.supplyAsync(helloWorldService::helloWorld) //this know as method reference.
                .thenApply(String::toUpperCase)
                .thenAccept((result) -> {
                    log("Result: " + result);
                }).join();
        //here above we chaining a multiple methods and this whole concept is called pipeline of completable future.

    }

    public CompletableFuture<String> HelloWorld() {
        HelloWorldService helloWorldService = new HelloWorldService();
        return CompletableFuture.supplyAsync(helloWorldService::helloWorld) //this know as method reference.
                .thenApply(String::toUpperCase)
                .thenApply(s -> s = s + "__" + s.length());
    }


    public String hellowold_multiple_calls() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());

        String name = hello_string
                .thenCombine(world_String, (hello, world) -> hello + world) // first and second completable future.
                .thenApply(String::toUpperCase)
                .join();
        timeTaken();
        return name;
    }


    public String three_multiple_functions_calls_thenCombine() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        });

        String name = hello_string
                .thenCombine(world_String, (hello, world) -> hello + world) // first and second completable future.
                .thenCombine(hi_String, (previous, current) -> current + "\n" + previous) //here previous means first thenCombine() and current means 'hi_string'.
                .thenApply(String::toUpperCase)
                .join();
        timeTaken();
        return name;
    }

    public CompletableFuture<String> using_thenCompose() {
        HelloWorldService helloWorldService = new HelloWorldService();
        return CompletableFuture.supplyAsync(() -> helloWorldService.hello())
                .thenCompose((prevoius) -> helloWorldService.worldFuture(prevoius)) // <- this is dependent task on it's previous hello() task, So the total time is summation of each and every independent task
                .thenApply(String::toUpperCase);
    }


    public String three_multiple_functions_calls_thenCombine_log() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        });

        String name = hello_string
                .thenCombine(world_String, (hello, world) -> {
                    log("then combine : hello / world");
                    return hello + world;
                }) // first and second completable future.
                .thenCombine(hi_String, (previous, current) -> {
                    log("then combine : current / previous");
                    return current + "\n" + previous;
                }) //here previous means first thenCombine() and current means 'hi_string'.
                .thenApply((string) -> {
                    log("then apply : string upper case");
                    return string.toUpperCase();
                })
                .join();
        timeTaken();
        return name;
    }


    public String three_multiple_functions_calls_thenCombine_user_define_threadpool() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello(), executorService);
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world(), executorService);
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        }, executorService);


        String name = hello_string
                .thenCombine(world_String, (hello, world) -> {
                    log("then combine : hello / world");
                    return hello + world;
                }) // first and second completable future.
                .thenCombine(hi_String, (previous, current) -> {
                    log("then combine : current / previous");
                    return current + "\n" + previous;
                }) //here previous means first thenCombine() and current means 'hi_string'.
                .thenApply((string) -> {
                    log("then apply : string upper case");
                    return string.toUpperCase();
                })
                .join();
        timeTaken();
        return name;
    }


    public String three_multiple_functions_calls_thenCombine_Async() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        });


        String name = hello_string
                .thenCombineAsync(world_String, (hello, world) -> {
                    log("then combine : hello / world");
                    return hello + world;
                }) // first and second completable future.
                .thenCombineAsync(hi_String, (previous, current) -> {
                    log("then combine : current / previous");
                    return current + "\n" + previous;
                }) //here previous means first thenCombine() and current means 'hi_string'.
                .thenApplyAsync((string) -> {
                    log("then apply : string upper case");
                    return string.toUpperCase();
                })
                .join();
        timeTaken();
        return name;
    }

    public String three_multiple_functions_calls_thenCombine_Async_withExecutor() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello(), executorService);
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world(), executorService);
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        }, executorService);


        String name = hello_string
                .thenCombineAsync(world_String, (hello, world) -> {
                    log("then combine : hello / world");
                    return hello + world;
                }, executorService) // first and second completable future.
                .thenCombineAsync(hi_String, (previous, current) -> {
                    log("then combine : current / previous");
                    return current + "\n" + previous;
                }, executorService) //here previous means first thenCombine() and current means 'hi_string'.
                .thenApplyAsync((string) -> {
                    log("then apply : string upper case");
                    return string.toUpperCase();
                }, executorService)
                .join();
        timeTaken();
        return name;
    }


    public String anyOf() {

        //db call
        CompletableFuture<String> db = CompletableFuture.supplyAsync(() -> {
            delay(1100);
            log("DB Call");
            return "Hello World!";
        });


        //rest call
        CompletableFuture<String> rest = CompletableFuture.supplyAsync(() -> {
            delay(1200);
            log("Rest Call");
            return "Hello World!";
        });


        //soap call
        CompletableFuture<String> soap = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            log("SOAP Call");
            return "Hello World!";
        });


        //CompletableFuture data = CompletableFuture.anyOf(db, rest, soap);
        //return data.join().toString();

        //OR
        List<CompletableFuture<String>> lst = List.of(soap, rest, db);
        CompletableFuture<Object> lst_anyOf = CompletableFuture.anyOf(lst.toArray(new CompletableFuture[lst.size()]));

        return (String) lst_anyOf.thenApply((v) -> {
                    if (v instanceof String) {
                        return v;
                    } else {
                        return null;
                    }
                })
                .join();


    }

}
