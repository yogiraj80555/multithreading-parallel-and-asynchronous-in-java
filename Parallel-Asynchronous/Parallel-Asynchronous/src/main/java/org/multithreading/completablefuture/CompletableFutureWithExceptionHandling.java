package org.multithreading.completablefuture;

import org.multithreading.service.HelloWorldService;

import java.util.concurrent.CompletableFuture;

import static org.multithreading.util.CommonUtil.*;
import static org.multithreading.util.LoggerUtil.log;

public class CompletableFutureWithExceptionHandling {

    private HelloWorldService helloWorldService;

    public CompletableFutureWithExceptionHandling(HelloWorldService helloWorldService) {
        this.helloWorldService = helloWorldService;
    }


    public String three_multiple_functions_calls_thenCombine_with_handle() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        });

        String name = hello_string
                .handle((previous,exception)-> {
                    log("Exception is: "+ exception);
                    return ""; // <- recoverable value.

                })
                .thenCombine(world_String, (hello,world) -> hello + world) //if exception occurs using recoverable value previous result will be empty string which we are returning.
                // o/p -> " world!" <- at this layer
                .thenCombine(hi_String, (previous , current) -> current + "\n" +previous)
                .thenApply(String::toUpperCase)
                .join();
        timeTaken();
        return name;
    }

    public String three_multiple_functions_calls_thenCombine_with_handle_world() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        });

        String name = hello_string
                .handle((previous,exception)-> { //hande function call even exception on occurs so below precaution.
                    if(exception!= null) {
                        log("Exception is: " + exception);
                        return ""; // <- recoverable value.
                    }else{
                        log("Result is: " + previous);
                        return previous;
                    }
                })
                .thenCombine(world_String, (hello,world) -> hello + world) //if exception occurs using recoverable value previous result will be empty string which we are returning.
                .handle((prev, exce)-> {
                    if(exce!= null) {
                        log("Second Exception: "+exce);
                        return "Exception encounterd";
                    }else{
                        log("Result is: " + prev);
                        return prev;
                    }

                })
                .thenCombine(hi_String, (previous , current) -> current + "\n" +previous)
                .thenApply(String::toUpperCase)
                .join();
        timeTaken();
        return name;
    }


    public String three_multiple_functions_calls_thenCombine_with_handle_world_exceptionlly() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        });

        String name = hello_string
                .exceptionally((exception)-> { //hande function call even exception on occurs so below precaution.
                        log("Exception is: " + exception);
                        return ""; // <- recoverable value.
                })
                .thenCombine(world_String, (hello,world) -> hello + world) //if exception occurs using recoverable value previous result will be empty string which we are returning.
                .exceptionally((exce)-> {
                        log("Second Exception: "+exce);
                        return "Exception encounterd";
                })
                .thenCombine(hi_String, (previous , current) -> current + "\n" +previous)
                .thenApply(String::toUpperCase)
                .join();
        timeTaken();
        return name;
    }

    public String three_multiple_functions_calls_thenCombine_with_WhenComplete() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        });

        String name = hello_string
                .whenComplete((previous,exception)-> {
                    log("Previous result is: "+ previous);
                    if(exception != null) {
                        log("Exception is: " + exception);
                    }
                })
                .thenCombine(world_String, (hello,world) -> hello + world) //if exception occurs using recoverable value previous result will be empty string which we are returning.
                .whenComplete((previous,exception)-> {
                    log("Previous after result is: "+ previous);
                    if(exception != null) {
                        log("Exception after is: " + exception);
                    }
                })
                .thenCombine(hi_String, (previous , current) -> current + "\n" +previous)
                .thenApply(String::toUpperCase)
                .join();
        timeTaken();
        return name;
    }

    public String three_multiple_functions_calls_thenCombine_with_WhenComplete_with_recover() {
        HelloWorldService helloWorldService = new HelloWorldService();
        startTimer();
        CompletableFuture<String> hello_string = CompletableFuture.supplyAsync(() -> helloWorldService.hello());
        CompletableFuture<String> world_String = CompletableFuture.supplyAsync(() -> helloWorldService.world());
        CompletableFuture<String> hi_String = CompletableFuture.supplyAsync(() -> {
            delay(1000);
            return "Hi I am 3rd future";
        });

        String name = hello_string
                .whenComplete((previous,exception)-> {
                    log("Previous result is: "+ previous);
                    if(exception != null) {
                        log("Exception is: " + exception);
                    }
                })
                .thenCombine(world_String, (hello,world) -> hello + world) //if exception occurs using recoverable value previous result will be empty string which we are returning.
                .whenComplete((previous,exception)-> {
                    log("Previous after result is: "+ previous);
                    if(exception != null) {
                        log("Exception after is: " + exception);
                    }
                })
                .exceptionally((e)->{
                    log("Exception after is: " + e);
                    return "Recovering Exception";
                })
                .thenCombine(hi_String, (previous , current) -> current + "\n" +previous)
                .thenApply(String::toUpperCase)
                .join();
        timeTaken();
        return name;
    }

}
