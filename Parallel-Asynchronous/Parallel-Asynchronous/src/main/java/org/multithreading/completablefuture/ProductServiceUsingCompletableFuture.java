package org.multithreading.completablefuture;

import org.multithreading.domain.*;
import org.multithreading.service.InventoryService;
import org.multithreading.service.ProductInfoService;
import org.multithreading.service.ReviewService;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static org.multithreading.util.CommonUtil.stopWatch;
import static org.multithreading.util.LoggerUtil.log;

public class ProductServiceUsingCompletableFuture {
    private ProductInfoService productInfoService;
    private ReviewService reviewService;

    private InventoryService inventoryService;
    public ProductServiceUsingCompletableFuture(ProductInfoService productInfoService, ReviewService reviewService) {
        this.productInfoService = productInfoService;
        this.reviewService = reviewService;
    }

    public ProductServiceUsingCompletableFuture(ProductInfoService productInfoService, ReviewService reviewService, InventoryService inventoryService) {
        this.productInfoService = productInfoService;
        this.reviewService = reviewService;
        this.inventoryService = inventoryService;
    }

    public Product retriveProductDetails(String productId) {

        stopWatch.start();

        //pros: this kind of code is very initiative and synchronous, Execuation of code goes step by step form top to bottom
        //cons: this kind of code is blocking nature mean that review service can only call after finishing product info call even though they both dependent on each other.
        //here we can improve the performance.
//        ProductInfo productInfo = productInfoService.retrieveProductInfo(productId); //blocking call
//        Review review = reviewService.retrieveReviews(productId); //blocking call

        log("Completable Future Approach 1");
        CompletableFuture<ProductInfo> productInfoCompletableFuture = CompletableFuture.supplyAsync(() -> productInfoService.retrieveProductInfo(productId));
        CompletableFuture<Review> reviewCompletableFuture = CompletableFuture.supplyAsync(() -> reviewService.retrieveReviews(productId));

        Product product = productInfoCompletableFuture
                .thenCombine(reviewCompletableFuture, (productInfo,review)-> new Product(productId,productInfo,review))
                .join();

        stopWatch.stop();
        log("Total Time Taken : "+ stopWatch.getTime());
        stopWatch.reset();
        //Product product = new Product(productId, productInfo, review);
        return product;

    }

    public CompletableFuture<Product> retriveProductDetailsApproach2(String productId) {

        log("Completable Future Approach 2");
        CompletableFuture<ProductInfo> productInfoCompletableFuture = CompletableFuture.supplyAsync(() -> productInfoService.retrieveProductInfo(productId));
        CompletableFuture<Review> reviewCompletableFuture = CompletableFuture.supplyAsync(() -> reviewService.retrieveReviews(productId));

        return productInfoCompletableFuture
                .thenCombine(reviewCompletableFuture, (productInfo,review)-> new Product(productId,productInfo,review));
    }


    public CompletableFuture<Product> retriveProductDetailsWithInventory(String productId) {


        CompletableFuture<ProductInfo> productInfoCompletableFuture = CompletableFuture
                .supplyAsync(() -> productInfoService.retrieveProductInfo(productId))
                .thenApply((productInfo -> {
                    productInfo.setProductOptions(updateProductInfoWithInventory(productInfo));
                    return productInfo;
                }));

        CompletableFuture<Review> reviewCompletableFuture = CompletableFuture.supplyAsync(() -> reviewService.retrieveReviews(productId));

        CompletableFuture<Product> product = productInfoCompletableFuture
                .thenCombine(reviewCompletableFuture, (productInfo,review)-> new Product(productId,productInfo,review))
                ;


        return product;

    }

    private List<ProductOption> updateProductInfoWithInventory(ProductInfo productInfo) {

        return productInfo.getProductOptions()
                .stream()
                .parallel()  //adding parallel here increace performance
                .map(productOption -> {
                    Inventory inventory = inventoryService.retriveInventory(productOption);
                    productOption.setInventory(inventory);
                    return productOption;
                })
                .collect(Collectors.toList());
    }




    public CompletableFuture<Product> retriveProductDetailsWithInventoryApproach2(String productId) {


        CompletableFuture<ProductInfo> productInfoCompletableFuture = CompletableFuture
                .supplyAsync(() -> productInfoService.retrieveProductInfo(productId))
                .thenApply((productInfo -> {
                    productInfo.setProductOptions(updateProductInfoWithInventoryApproach2(productInfo));
                    return productInfo;
                }));

        CompletableFuture<Review> reviewCompletableFuture = CompletableFuture
                .supplyAsync(() -> reviewService.retrieveReviews(productId))
                .exceptionally((e) -> {
                    log("Handling exception in Review Service: "+e.getMessage());
                    return Review.builder()
                            .noOfReviews(0)
                            .overallRating(0.0).build();
                });

        CompletableFuture<Product> product = productInfoCompletableFuture
                .thenCombine(reviewCompletableFuture, (productInfo,review)-> new Product(productId,productInfo,review))
                .whenComplete((previousProduct, ex) -> {
                    log("Inside WhenComplete Product is: " + previousProduct);
                    if(ex != null) {
                        log("Exception is: " + ex);
                    }
                });

        return product;

    }
    private List<ProductOption> updateProductInfoWithInventoryApproach2(ProductInfo productInfo) {

        //this technique is used in nested blocking calls.

     List<CompletableFuture<ProductOption>> optionCompletableFuture = productInfo.getProductOptions()
              .stream()
              .map((productOption) -> {
                  return CompletableFuture.supplyAsync(()-> inventoryService.retriveInventory(productOption))
                          .thenApply(inventory -> {
                              productOption.setInventory(inventory);
                              return productOption;
                          })
                          .exceptionally((e)->{
                              log("Exception in inventory retriving: "+e.getMessage());
                              productOption.setInventory(Inventory.builder().count(999999).build());
                              return productOption;
                          });
              })
             .collect(Collectors.toList());
     return optionCompletableFuture.stream().map(CompletableFuture::join).collect(Collectors.toList());
    }


    public static void main(String args []) {
        ProductInfoService infoService = new ProductInfoService();
        ReviewService reviewService = new ReviewService();
        ProductServiceUsingCompletableFuture productService = new ProductServiceUsingCompletableFuture(infoService,reviewService);
        String productId = "ABCD123";
        Product product = productService.retriveProductDetails(productId);
        log("Product is: "+ product);
    }

}
