package org.multithreading.util;

import org.apache.commons.lang3.time.StopWatch;
import static java.lang.Thread.sleep;
import static org.multithreading.util.LoggerUtil.log;


public class CommonUtil {

    public static StopWatch stopWatch = new StopWatch();

    public static void delay (long miliseconds) {
        try{
            sleep(miliseconds);
        } catch (InterruptedException e) {
            log("Exception is :" + e.getMessage());
        }
    }

    public static String transform(String s) {
        CommonUtil.delay(500);
        return s.toUpperCase();
    }

    public static void stopWatchReset() {
        stopWatch.reset();
    }

    public static void startTimer(){
        stopWatchReset();
        stopWatch.start();
    }

    public static void timeTaken() {
        stopWatch.stop();
        log("Time taken: " + stopWatch.getTime());
    }

    public static int noOfCores() {
        return Runtime.getRuntime().availableProcessors();
    }

}
