package org.multithreading.util;

import org.multithreading.domain.checkout.Cart;
import org.multithreading.domain.checkout.CartItem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class Dataset {

    public static List<String> namedList() {
        return List.of("Bob", "mery", "Jems", "Dravid", "Aryan", "Mark", "Henrik");
    }


    public static Cart createCart(int nosItems) {
        Cart cart = new Cart();
        List<CartItem> cartItemList = new ArrayList<>();
        IntStream.range(1,nosItems)
                .forEach((index)->{
                    String cartItem = "CartItem ".concat(index+"");
                    CartItem item = new CartItem(index, cartItem, generateRandomPrice(), generateRandomquantity(), false);
                    cartItemList.add(item);
                });
        cart.setCartItemList(cartItemList);
        return cart;
    }

    private static double generateRandomPrice() {
        return Math.random() * (100 - 10 + 1) - 10;
    }

    private static Integer generateRandomquantity() {
        return new Integer((int) (Math.random() * (30 - 1 + 1) - 1));
    }


    public static ArrayList<Integer> generateRandomNumbers(int size) {
        ArrayList<Integer>  arrayList = new ArrayList<>();

        IntStream.rangeClosed(1,size)
                .boxed()
                .forEach((arrayList::add));
        return arrayList;
    }

    public static LinkedList<Integer> generateLinkedList(int size) {
        LinkedList<Integer> linkedList = new LinkedList<>();

        IntStream.rangeClosed(1,size)
                .boxed()
                .forEach((linkedList::add));
        return linkedList;
    }

}
