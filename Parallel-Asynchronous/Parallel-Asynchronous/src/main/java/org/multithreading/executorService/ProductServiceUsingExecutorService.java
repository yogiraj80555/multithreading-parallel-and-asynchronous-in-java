package org.multithreading.executorService;

import org.multithreading.domain.Product;
import org.multithreading.domain.ProductInfo;
import org.multithreading.domain.Review;
import org.multithreading.service.ProductInfoService;
import org.multithreading.service.ReviewService;

import java.util.concurrent.*;

import static org.multithreading.util.CommonUtil.stopWatch;
import static org.multithreading.util.LoggerUtil.log;

public class ProductServiceUsingExecutorService {
    private ProductInfoService productInfoService;
    private ReviewService reviewService;
    private static ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public ProductServiceUsingExecutorService(ProductInfoService productInfoService, ReviewService reviewService) {
        this.productInfoService = productInfoService;
        this.reviewService = reviewService;
    }

    public Product retriveProductDetails(String productId) throws ExecutionException, InterruptedException, TimeoutException {

        stopWatch.start();


        Future<ProductInfo> productInfoFuture = executorService.submit(() -> productInfoService.retrieveProductInfo(productId));
        Future<Review> reviewFuture = executorService.submit(() -> reviewService.retrieveReviews(productId));

        // Here if we give 1 sec as timeout it will give us timeout exception
        // Because we asked to wait 1 set for product retrieve info function
        // also not that get function blocking the caller till tasks gets executes.
        // and here for 2 secs.
        // also there is no better way to merge 2 Futures.
        ProductInfo productInfo = productInfoFuture.get(2,TimeUnit.SECONDS);
        Review review = reviewFuture.get();

        stopWatch.stop();
        log("Total Time Taken : "+ stopWatch.getTime());
        Product product = new Product(productId, productInfo, review);
        return product;

    }

    public static void main(String args []) throws ExecutionException, InterruptedException, TimeoutException {
        ProductInfoService infoService = new ProductInfoService();
        ReviewService reviewService = new ReviewService();
        ProductServiceUsingExecutorService productService = new ProductServiceUsingExecutorService(infoService,reviewService);
        String productId = "ABCD123";
        Product product = productService.retriveProductDetails(productId);
        log("Product is: "+ product);

        //Remember if you are using executor service the thread pool will not trun off after finishing program you need to do it maually
        executorService.shutdown();
    }

}
