package org.multithreading.fork_join_framework;

import org.apache.commons.lang3.time.StopWatch;
import org.multithreading.util.Dataset;

import java.util.ArrayList;
import java.util.List;

import static org.multithreading.util.CommonUtil.delay;
import static org.multithreading.util.CommonUtil.stopWatch;
import static org.multithreading.util.LoggerUtil.log;

public class StringTransform {


    public static void main(String args[]) {

        stopWatch.start();
        List<String> result = new ArrayList<>();
        List<String> names = Dataset.namedList();

        names.forEach((name)->{
            String transformName = nameLengthTransform(name);
            result.add(transformName);
        });
        stopWatch.stop();
        log("Final Result: "+ result);
        log("Total time taken: "+ stopWatch.getTime());
    }

    private static String nameLengthTransform(String name){
        delay(1000);
        return name.length() + " --- " + name;
    }
}
