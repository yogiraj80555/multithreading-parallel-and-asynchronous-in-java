package org.multithreading.fork_join_framework;

import org.multithreading.util.Dataset;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

import static org.multithreading.util.CommonUtil.delay;
import static org.multithreading.util.CommonUtil.stopWatch;
import static org.multithreading.util.LoggerUtil.log;

public class ForkJoinStringTransformUsingRecursion extends RecursiveTask<List<String>> {

    private List<String> inputList;

    public ForkJoinStringTransformUsingRecursion(List<String> inputList) {
        this.inputList = inputList;
    }

    public static void main(String args[]) {

        stopWatch.start();
        List<String> result = new ArrayList<>();
        List<String> names = Dataset.namedList();
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        ForkJoinStringTransformUsingRecursion forkJoinTask = new ForkJoinStringTransformUsingRecursion(names); //since we extend it with ResursiveTask so it become ForkJoinTask
        result = forkJoinPool.invoke(forkJoinTask);//here actual task is going to be added in shared queue.
        stopWatch.stop();
        log("Final Result: "+ result);
        log("Total time taken: "+ stopWatch.getTime());
    }

    private static String nameLengthTransform(String name){
        delay(1000);
        return name.length() + " --- " + name;
    }

    @Override
    protected List<String> compute() {
        //here all fork join magic is going to be happened
        //here we are writing fork and join operations

        //braking the recursive condition.
        if (inputList.size() <= 1) {
            List<String> result = new ArrayList<>();
            inputList.forEach((name)-> result.add(nameLengthTransform(name)));
            return result;
        }

        int mid_element = inputList.size()/2;
        List<String> leftList = inputList.subList(0,mid_element);
        List<String> rightList = inputList.subList(mid_element, inputList.size());

        //fork() is the one which is going to add the particular task into the actual work queue of working threads.
        ForkJoinTask<List<String>> leftInputList = new ForkJoinStringTransformUsingRecursion(leftList).fork(); //fork arranges to asynchronously execute this task in the pool the current task is running in
        inputList = rightList;
        List<String> rightResult = compute(); //here recursion is happening
        List<String> leftResult = leftInputList.join();
        leftResult.addAll(rightResult);
        return leftResult;
    }
}
