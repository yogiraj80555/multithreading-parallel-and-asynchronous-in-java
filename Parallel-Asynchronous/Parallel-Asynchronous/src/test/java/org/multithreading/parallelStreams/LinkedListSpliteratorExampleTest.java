package org.multithreading.parallelStreams;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.multithreading.util.Dataset;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.multithreading.util.LoggerUtil.log;

class LinkedListSpliteratorExampleTest {

    @RepeatedTest(5)
    void multiplayEachValueSequential() {
        LinkedListSpliteratorExample linkedListSpliteratorExample = new LinkedListSpliteratorExample();
        LinkedList<Integer> inputData = Dataset.generateLinkedList(1000000);

        List<Integer> result = linkedListSpliteratorExample.multiplayEachValue(inputData,2,false);

        assertEquals(inputData.size(),result.size());



    }

    @RepeatedTest(5)
    void multiplayEachValueParallel() {
        LinkedListSpliteratorExample linkedListSpliteratorExample = new LinkedListSpliteratorExample();
        LinkedList<Integer> inputData = Dataset.generateLinkedList(1000000);

        List<Integer> result = linkedListSpliteratorExample.multiplayEachValue(inputData,2,true);
        log("Linked List is a type of collection which is really difficult to split into individual chunks");
        log("Always test your code performance before using parallel stream");

        assertEquals(inputData.size(),result.size());

    }
}