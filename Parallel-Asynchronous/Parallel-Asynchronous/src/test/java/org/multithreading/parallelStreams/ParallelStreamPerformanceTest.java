package org.multithreading.parallelStreams;

import org.junit.jupiter.api.Test;
import org.multithreading.util.Dataset;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ParallelStreamPerformanceTest {

    ParallelStreamPerformance parallelStreamPerformance = new ParallelStreamPerformance();

    @Test
    void sum_using_intStream() {

        //serial
        System.out.println("Serial ->");
        int sum = parallelStreamPerformance.sum_using_intstram(1000000,false);
        System.out.println("Sum: "+sum);
        assertEquals(1784293664,sum);

        //parallal
        System.out.println("Parallel ->");
        sum = parallelStreamPerformance.sum_using_intstram(1000000,true);
        System.out.println("Sum: "+sum);
        assertEquals(1784293664,sum);
    }

    @Test
    void sum_using_iterate() {

        //serial
        System.out.println("Serial ->");
        int sum = parallelStreamPerformance.sum_using_iterate(1000000,false);
        System.out.println("Sum: "+sum);
        assertEquals(1784293664,sum);

        //parallal
        System.out.println("Parallel ->");
        sum = parallelStreamPerformance.sum_using_iterate(1000000,true);
        System.out.println("Sum: "+sum);
        assertEquals(1784293664,sum);
    }


    @Test
    void sum_using_list() {

        int size = 1000000;
        ArrayList<Integer> inputData = Dataset.generateRandomNumbers(size);

        //serial
        System.out.println("Serial ->");
        int sum = parallelStreamPerformance.sum_using_list(inputData,false);
        System.out.println("Sum: "+sum);
        assertEquals(1784293664,sum);

        //parallal
        System.out.println("Parallel ->");
        sum = parallelStreamPerformance.sum_using_list(inputData,true);
        System.out.println("Sum: "+sum);
        assertEquals(1784293664,sum);
    }

}