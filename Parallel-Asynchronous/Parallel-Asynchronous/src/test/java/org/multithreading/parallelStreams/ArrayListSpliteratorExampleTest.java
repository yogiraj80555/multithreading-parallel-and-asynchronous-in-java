package org.multithreading.parallelStreams;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.multithreading.util.Dataset;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.multithreading.util.LoggerUtil.log;

class ArrayListSpliteratorExampleTest {

    @RepeatedTest(5)
    void multiplayEachValueSequential() {
        ArrayListSpliteratorExample arrayListSpliteratorExample = new ArrayListSpliteratorExample();
        ArrayList<Integer> inputData = Dataset.generateRandomNumbers(1000000);

       List<Integer> result = arrayListSpliteratorExample.multiplayEachValue(inputData,2,false);

        assertEquals(inputData.size(),result.size());



    }

    @RepeatedTest(5)
    void multiplayEachValueParallel() {
        ArrayListSpliteratorExample arrayListSpliteratorExample = new ArrayListSpliteratorExample();
        ArrayList<Integer> inputData = Dataset.generateRandomNumbers(1000000);

        List<Integer> result = arrayListSpliteratorExample.multiplayEachValue(inputData,2,true);
        log("Array list is based on index based slicing so we could not see bigger difference between sequential and parallel stream here");

        assertEquals(inputData.size(),result.size());

    }
}