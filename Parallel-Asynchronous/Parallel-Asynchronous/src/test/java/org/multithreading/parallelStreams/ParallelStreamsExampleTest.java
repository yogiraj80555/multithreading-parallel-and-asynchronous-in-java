package org.multithreading.parallelStreams;

import lombok.Value;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.multithreading.util.CommonUtil;
import org.multithreading.util.Dataset;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParallelStreamsExampleTest {

    ParallelStreamsExample parallelStreamsExample = new ParallelStreamsExample();

    @Test
    void transformUsingSequentialStream() {
        //given
        List<String> names = Dataset.namedList();

        //when
        CommonUtil.startTimer();
        List<String> result = parallelStreamsExample.transformUsingSequentialStream(names);
        CommonUtil.timeTaken();

        //then
        assertEquals(names.size(),result.size());
        result.forEach((name) -> {
            assertTrue(name.contains("---"));
        });
    }

    @Test
    void transformUsingParallelStream() {
        //given
        List<String> names = Dataset.namedList();

        //when
        CommonUtil.startTimer();
        List<String> result = parallelStreamsExample.transformUsingParallelStream(names);
        CommonUtil.timeTaken();

        //then
        assertEquals(names.size(),result.size());
        result.forEach((name)-> {
            assertTrue(name.contains("---"));
        });
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void transformDynamicParallelSequentialStream(boolean value) {
        //given
        List<String> names = Dataset.namedList();

        //when
        CommonUtil.startTimer();
        List<String> result = parallelStreamsExample.transformDynamic_Sequential_Parallel(names,value);
        CommonUtil.timeTaken();

        //then
        assertEquals(names.size(),result.size());
        result.forEach((name)-> {
            assertTrue(name.contains("---"));
        });
    }

}