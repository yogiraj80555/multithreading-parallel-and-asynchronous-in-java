package org.multithreading.completablefuture;

import org.junit.jupiter.api.Test;
import org.multithreading.domain.Product;
import org.multithreading.service.InventoryService;
import org.multithreading.service.ProductInfoService;
import org.multithreading.service.ReviewService;

import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;
import static org.multithreading.util.CommonUtil.stopWatch;
import static org.multithreading.util.CommonUtil.timeTaken;
import static org.multithreading.util.LoggerUtil.log;

class ProductServiceUsingCompletableFutureTest {

    @Test
    void retriveProductDetails() {

        ProductInfoService infoService = new ProductInfoService();
        ReviewService reviewService = new ReviewService();
        ProductServiceUsingCompletableFuture productService = new ProductServiceUsingCompletableFuture(infoService,reviewService);
        String productId = "ABCD123";
        Product product = productService.retriveProductDetails(productId);


        assertNotNull(product);
        assertEquals(product.getProductId(), productId);
    }


    @Test
    void retriveProductDetailsApproach2() {

        ProductInfoService infoService = new ProductInfoService();
        ReviewService reviewService = new ReviewService();
        ProductServiceUsingCompletableFuture productService = new ProductServiceUsingCompletableFuture(infoService,reviewService);
        String productId = "ABCD123";
        stopWatch.start();
        CompletableFuture<Product> productCF = productService.retriveProductDetailsApproach2(productId);
        Product product = productCF.join();
        timeTaken();
        stopWatch.reset();

        assertNotNull(product);
        assertEquals(product.getProductId(), productId);
        log("Product is: "+ product);
    }


    @Test
    void retriveProductDetailsWithInventory() {
        InventoryService inventoryService = new InventoryService();
        ProductInfoService infoService = new ProductInfoService();
        ReviewService reviewService = new ReviewService();
        ProductServiceUsingCompletableFuture productService = new ProductServiceUsingCompletableFuture(infoService,reviewService,inventoryService);
        String productId = "ABCD123";
        stopWatch.start();
        CompletableFuture<Product> productCF = productService.retriveProductDetailsWithInventory(productId);
        Product product = productCF.join();
        timeTaken();
        stopWatch.reset();

        assertNotNull(product);
        assertEquals(product.getProductId(), productId);
        product.getProductInfo().getProductOptions().forEach(productOption -> {
            assertNotNull(productOption.getInventory());
        });
        log("Product is: "+ product);
    }

    @Test
    void retriveProductDetailsWithInventoryApproach2() {
        InventoryService inventoryService = new InventoryService();
        ProductInfoService infoService = new ProductInfoService();
        ReviewService reviewService = new ReviewService();
        ProductServiceUsingCompletableFuture productService = new ProductServiceUsingCompletableFuture(infoService,reviewService,inventoryService);
        String productId = "ABCD123";
        stopWatch.start();
        CompletableFuture<Product> productCF = productService.retriveProductDetailsWithInventoryApproach2(productId);
        Product product = productCF.join();
        timeTaken();
        stopWatch.reset();

        assertNotNull(product);
        assertEquals(product.getProductId(), productId);
        product.getProductInfo().getProductOptions().forEach(productOption -> {
            assertNotNull(productOption.getInventory());
        });
        log("Product is: "+ product);
    }
}