package org.multithreading.completablefuture;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.multithreading.service.HelloWorldService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.multithreading.util.LoggerUtil.log;

@ExtendWith(MockitoExtension.class)
class CompletableFutureWithExceptionHandlingTest {


    @Mock
    HelloWorldService helloWorldService = mock(HelloWorldService.class);
    @InjectMocks
    CompletableFutureWithExceptionHandling completableFutureWithExceptionHandling;


    @Test
    public void three_multiple_functions_calls_thenCombine_with_handle(){

        when(helloWorldService.hello()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));
        when(helloWorldService.world()).thenCallRealMethod();

        String result = completableFutureWithExceptionHandling.three_multiple_functions_calls_thenCombine_with_handle();
        log(result);
    }

    @Test
    public void three_multiple_functions_calls_thenCombine_with_handle_world(){

        when(helloWorldService.hello()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));
        when(helloWorldService.world()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));

        String result = completableFutureWithExceptionHandling.three_multiple_functions_calls_thenCombine_with_handle_world();
        log(result);
    }

    @Test
    public void three_multiple_functions_calls_thenCombine_with_handle_world_success(){

        when(helloWorldService.hello()).thenCallRealMethod();
        when(helloWorldService.world()).thenCallRealMethod();

        String result = completableFutureWithExceptionHandling.three_multiple_functions_calls_thenCombine_with_handle_world();
        log(result);
    }


    @Test
    public void three_multiple_functions_calls_thenCombine_with_handle_world_success_exceptionally_happy_path(){

        when(helloWorldService.hello()).thenCallRealMethod();
        when(helloWorldService.world()).thenCallRealMethod();

        String result = completableFutureWithExceptionHandling.three_multiple_functions_calls_thenCombine_with_handle_world_exceptionlly();
        log(result);
    }

    @Test
    public void three_multiple_functions_calls_thenCombine_with_handle_world_exceptionally(){

        when(helloWorldService.hello()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));
        when(helloWorldService.world()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));

        String result = completableFutureWithExceptionHandling.three_multiple_functions_calls_thenCombine_with_handle_world_exceptionlly();
        log(result);
    }

    @Test
    public void three_multiple_functions_calls_thenCombine_with_handle_world_WhenComplete(){

        when(helloWorldService.hello()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));
        when(helloWorldService.world()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));

        String result = completableFutureWithExceptionHandling.three_multiple_functions_calls_thenCombine_with_WhenComplete();
        log(result);
    }

    @Test
    public void three_multiple_functions_calls_thenCombine_with_handle_world_WhenComplete_happy(){

        when(helloWorldService.hello()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));
        when(helloWorldService.world()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));

        String result = completableFutureWithExceptionHandling.three_multiple_functions_calls_thenCombine_with_WhenComplete();
        log(result);
    }

    @Test
    public void three_multiple_functions_calls_thenCombine_with_handle_world_WhenComplete_with_recover(){

        when(helloWorldService.hello()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));
        when(helloWorldService.world()).thenThrow(new RuntimeException("Runtime custom Exception Occurs"));

        String result = completableFutureWithExceptionHandling.three_multiple_functions_calls_thenCombine_with_WhenComplete_with_recover();
        log(result);
    }

}