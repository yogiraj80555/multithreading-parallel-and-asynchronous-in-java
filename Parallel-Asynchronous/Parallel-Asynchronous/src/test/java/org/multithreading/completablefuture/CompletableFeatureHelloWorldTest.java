package org.multithreading.completablefuture;

import org.junit.jupiter.api.Test;
import org.multithreading.util.CommonUtil;

import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;
import static org.multithreading.util.CommonUtil.startTimer;
import static org.multithreading.util.CommonUtil.timeTaken;
import static org.multithreading.util.LoggerUtil.log;

class CompletableFeatureHelloWorldTest {

    CompletableFeatureHelloWorld completableFeatureHelloWorld = new CompletableFeatureHelloWorld();

    @Test
    void helloWorld() {

    CompletableFuture<String> future = completableFeatureHelloWorld.HelloWorld();

    String word = "HELLO WORLD!";

    future.thenAccept( s -> {
        assertEquals(word+"__"+word.length(),s);
        }).join();
    }

    @Test
    void helloworld_with_thenCombine() {
        String world = completableFeatureHelloWorld.hellowold_multiple_calls();
        String word = "HELLO WORLD!";
        assertEquals(word,world);
    }

    @Test
    void three_multiple_functions_calls_thenCombineTest() {
        String world = completableFeatureHelloWorld.three_multiple_functions_calls_thenCombine();
        assertEquals("HI I AM 3RD FUTURE\nHELLO WORLD!",world);

    }

    @Test
    void helloWorld_thenCompose() {
        startTimer();
        CompletableFuture<String> future = completableFeatureHelloWorld.using_thenCompose();

        String word = "HELLO WORLD!";

        future.thenAccept( s -> {
            timeTaken();
            assertEquals(word,s);
        }).join();
    }



    @Test
    void three_multiple_functions_calls_thenCombine_user_define_threadpool() {
        log("Check Logger\n here all code in completable future is getting executed into user defined thread Pool, known as ExecutorService\n" +
                "See the thread name it starting from -> [pool-<>-thread-<>] <- like this");
        String world = completableFeatureHelloWorld.three_multiple_functions_calls_thenCombine_user_define_threadpool();
        assertEquals("HI I AM 3RD FUTURE\nHELLO WORLD!",world);
    }

    @Test
    void three_multiple_functions_calls_thenCombineTest_log() {
        log("Check Logger\n here all code in completable future is getting executed into Fork Join Pool-CommonPool");
        String world = completableFeatureHelloWorld.three_multiple_functions_calls_thenCombine_log();
        assertEquals("HI I AM 3RD FUTURE\nHELLO WORLD!",world);

        /**
         * Output:-
         * [main] - Check Logger
         *  here all code in completable future is getting executed into Fork Join Pool-CommonPool
         * [ForkJoinPool.commonPool-worker-1] - Inside Hello
         * [ForkJoinPool.commonPool-worker-2] - inside world
         * [ForkJoinPool.commonPool-worker-2] - then combine : hello / world
         * [ForkJoinPool.commonPool-worker-2] - then combine : current / previous
         * [ForkJoinPool.commonPool-worker-2] - then apply : string upper case
         * [main] - Time taken: 1009
         *
         *
         * Here above we saw "...commonPool-worker-1] - Inside Hello" and then all "...commonPool-worker-1] - ....." are on 'worker-1' is this by design basically
         * the reason why pipeline maintains the same thread is that sometimes context switching might include some cost to the overall performance under certain scenarios
         * that is why API designed to use same thread that executed the previous step in completable future pipeline.
         *
         * But we can alter this behaviour.
         * see below........
         */
    }


    @Test
    void three_multiple_functions_calls_thenCombine_Async() {
        log("Check Logger\n With Async function each job tries to execute on different thread if context switching will not dropping performance");
        String world = completableFeatureHelloWorld.three_multiple_functions_calls_thenCombine_Async();
        assertEquals("HI I AM 3RD FUTURE\nHELLO WORLD!",world);
    }

    @Test
    void three_multiple_functions_calls_thenCombine_Async_withExecutor() {
        log("Check Logger\n With Async function with executor service each job tries to execute on different thread");
        String world = completableFeatureHelloWorld.three_multiple_functions_calls_thenCombine_Async_withExecutor();
        assertEquals("HI I AM 3RD FUTURE\nHELLO WORLD!",world);
    }


    @Test
    void anyOf() {
        log("Completable Future anyOf function");
        String world = completableFeatureHelloWorld.anyOf();
        assertEquals("Hello World!",world);
    }

}