package org.multithreading.completablefuture;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.multithreading.domain.Product;
import org.multithreading.service.InventoryService;
import org.multithreading.service.ProductInfoService;
import org.multithreading.service.ReviewService;

import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.multithreading.util.LoggerUtil.log;

class ProductServiceUsingCompletableFutureExceptionHandlingTest extends MockitoExtension {



    @Test
    void retriveProductDetailsWithInventoryApproach2() {

        String productId = "ABCD1234";

        ProductInfoService productInfoService = Mockito.mock(ProductInfoService.class);
        ReviewService reviewService = Mockito.mock(ReviewService.class);
        InventoryService inventoryService = Mockito.mock(InventoryService.class);
        ProductServiceUsingCompletableFuture psucFuture =
                new ProductServiceUsingCompletableFuture(productInfoService, reviewService, inventoryService);
        when(productInfoService.retrieveProductInfo(productId)).thenCallRealMethod();
        when(reviewService.retrieveReviews(any())).thenThrow(new NullPointerException("This is Null Pointer Exception"));
        when(inventoryService.retriveInventory(any())).thenCallRealMethod();


        CompletableFuture<Product> productFuture = psucFuture.retriveProductDetailsWithInventoryApproach2(productId);
        Product product = productFuture.join();
        assertNotNull(product.getReview());
        assertEquals(0, product.getReview().getNoOfReviews());
        log("Product is: "+ product);

    }


    @Test
    void retriveProductDetailsWithInventoryException() {

        String productId = "ABCD1234";

        ProductInfoService productInfoService = Mockito.mock(ProductInfoService.class);
        ReviewService reviewService = Mockito.mock(ReviewService.class);
        InventoryService inventoryService = Mockito.mock(InventoryService.class);
        ProductServiceUsingCompletableFuture psucFuture =
                new ProductServiceUsingCompletableFuture(productInfoService, reviewService, inventoryService);
        when(productInfoService.retrieveProductInfo(productId)).thenCallRealMethod();
        when(reviewService.retrieveReviews(any())).thenCallRealMethod();
        when(inventoryService.retriveInventory(any())).thenThrow(new NullPointerException("This is Null Pointer Exception"));


        CompletableFuture<Product> productFuture = psucFuture.retriveProductDetailsWithInventoryApproach2(productId);
        Product product = productFuture.join();
        assertNotNull(product.getReview());
        //assertEquals(0, product.getReview().getNoOfReviews());
        log("Product is: "+ product);

    }

    @Test
    void retriveProductDetailsWithInventoryApproach2_with_productException() {

        String productId = "ABCD1234";

        ProductInfoService productInfoService = Mockito.mock(ProductInfoService.class);
        ReviewService reviewService = Mockito.mock(ReviewService.class);
        InventoryService inventoryService = Mockito.mock(InventoryService.class);
        ProductServiceUsingCompletableFuture psucFuture =
                new ProductServiceUsingCompletableFuture(productInfoService, reviewService, inventoryService);

        when(productInfoService.retrieveProductInfo(any())).thenThrow(new NullPointerException("This is Product Null Pointer Exception"));
        when(reviewService.retrieveReviews(any())).thenThrow(new NullPointerException("This is Null Pointer Exception"));
        when(inventoryService.retriveInventory(any())).thenCallRealMethod();

        Assertions.assertThrows(NullPointerException.class,() -> psucFuture.retriveProductDetailsWithInventoryApproach2(productId).join());

    }
}