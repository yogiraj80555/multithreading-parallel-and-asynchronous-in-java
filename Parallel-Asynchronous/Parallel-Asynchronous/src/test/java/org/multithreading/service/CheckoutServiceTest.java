package org.multithreading.service;

import org.junit.jupiter.api.Test;
import org.multithreading.domain.checkout.Cart;
import org.multithreading.domain.checkout.CheckoutResponse;
import org.multithreading.domain.checkout.CheckoutStatus;
import org.multithreading.util.Dataset;

import java.util.concurrent.ForkJoinPool;

import static org.junit.jupiter.api.Assertions.*;
import static org.multithreading.util.LoggerUtil.log;

public class CheckoutServiceTest {

    PriceValidatorService priceValidatorService = new PriceValidatorService();
    CheckoutService checkoutService = new CheckoutService(priceValidatorService);

    @Test
    void checkout() {
        //given
        Cart cart = Dataset.createCart(16); //see value 15,16,17,18,19  for cores difference execution.
        //when
        CheckoutResponse checkoutResponse = checkoutService.checkout(cart);
        //then
        log("Status: "+ checkoutResponse.getCheckoutStatus()+"");
        if(checkoutResponse.getCheckoutStatus() ==  CheckoutStatus.SUCCESS){
            log("Price: "+ checkoutResponse.getFinalRate()+"");
        }
        log("Number of Cores: "+Runtime.getRuntime().availableProcessors());

    }

    @Test
    void parallelism() {
        System.out.println("Parallelism: "+ ForkJoinPool.getCommonPoolParallelism());
    }


    @Test
    void modify_parallelism() {
        //changing the default parallelism value
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "100"); //this enabled it will take 1047 time, if it disabled parallelism will take 7047 time
        // "100" means it is running all 100 task parallel
        //when we have change like this we are impacting whole common fork join pool for entire app process.

        //given
        Cart cart = Dataset.createCart(100); //see value 15,16,17,18,19  for cores difference execution.
        //when
        CheckoutResponse checkoutResponse = checkoutService.checkout(cart);
        //then
        log("Status: "+ checkoutResponse.getCheckoutStatus()+"");

    }
}
