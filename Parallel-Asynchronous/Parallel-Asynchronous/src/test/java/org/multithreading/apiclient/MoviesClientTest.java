package org.multithreading.apiclient;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.multithreading.util.CommonUtil.startTimer;
import static org.multithreading.util.CommonUtil.timeTaken;
import static org.multithreading.util.LoggerUtil.log;

class MoviesClientTest {

    WebClient webClient = WebClient.builder()
            .baseUrl("http://127.0.0.1:8080/movies")
            .build();

    MoviesClient client = new MoviesClient(webClient);


    //Note: remember 1st network call is always take in Webclient after that webClient reuses previous connection for call
    @RepeatedTest(10)
    void retrieveMovie() {
        //need to see some instruction from MoviesClient.retriveMovie() function before running this test case
        var movieId = 2L;

        startTimer();
        var movie = client.retriveMovie(movieId);
        timeTaken();
        log(""+movie);
        assert movie != null;
        assertEquals("The Dark Knight",movie.getMovieInfo().getName());
        assertEquals("2",movie.getReviewList().get(0).getMovieInfoId());
    }



    @RepeatedTest(10)
    void retrieveMovie_with_CompletableFuture() {
        //need to see some instruction from MoviesClient.retriveMovie() function before running this test case
        var movieId = 2L;

        startTimer();
        var movie = client.retriveMovie_with_CompletableFuture(movieId).join();
        timeTaken();
        log(""+movie);
        assert movie != null;
        assertEquals("The Dark Knight",movie.getMovieInfo().getName());
        assertEquals("2",movie.getReviewList().get(0).getMovieInfoId());
    }




    @RepeatedTest(10)
    void retrieveMovies() {
        //need to see some instruction from MoviesClient.retriveMovie() function before running this test case
        var movieId = List.of(1L,2L,3L,4L,5L,6L,7L);

        startTimer();
        var movie = client.retrieveMovies(movieId);
        timeTaken();
        log(""+movie);
        assert movie != null;
        assertEquals("The Dark Knight",movie.get(1).getMovieInfo().getName());
        assertEquals("2",movie.get(1).getReviewList().get(0).getMovieInfoId());
    }


    @RepeatedTest(10)
    void retrieveMovie_with_CompletableFuture_list() {
        //need to see some instruction from MoviesClient.retriveMovie() function before running this test case
        var movieId = List.of(1L,2L,3L,4L,5L,6L,7L);;

        startTimer();
        var movie = client.retriveMovie_with_CompletableFuture(movieId);
        timeTaken();
        log(""+movie);
        assert movie != null;
        assertEquals("The Dark Knight",movie.get(1).getMovieInfo().getName());
        assertEquals("2",movie.get(1).getReviewList().get(0).getMovieInfoId());
    }


    @RepeatedTest(10)
    void retrieveMovie_with_CompletableFuture_allOf() {
        //need to see some instruction from MoviesClient.retriveMovie() function before running this test case
        var movieId = List.of(1L,2L,3L,4L,5L,6L,7L);;

        startTimer();
        var movie = client.retriveMovie_with_CompletableFuture_allOf(movieId);
        timeTaken();
        log(""+movie);
        assert movie != null;
        assertEquals("The Dark Knight",movie.get(1).getMovieInfo().getName());
        assertEquals("2",movie.get(1).getReviewList().get(0).getMovieInfoId());
    }
}